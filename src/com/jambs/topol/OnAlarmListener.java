package com.jambs.topol;

public interface OnAlarmListener {
    public void onAlarm(Alarm alarm);
}
