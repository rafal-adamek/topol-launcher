package com.jambs.topol.theming;

import android.content.Context;
import android.preference.DialogPreference;
import android.util.AttributeSet;

import com.jambs.topol.R;

/**
 * Created by r.adamek on 5/8/17.
 */

public class ColorDialogPreference extends DialogPreference {

    public ColorDialogPreference(Context context, AttributeSet attrs) {
        super(context, attrs);

        setDialogLayoutResource(R.layout.color_picker_dialog);
        setPositiveButtonText(android.R.string.ok);
        setNegativeButtonText(android.R.string.cancel);

        setDialogIcon(null);

    }

}
