package com.jambs.topol.theming;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.content.ContextCompat;

import com.jambs.topol.LauncherFiles;
import com.jambs.topol.R;

/**
 * Created by r.adamek on 5/4/17.
 */

public class ThemeManager {

    private SharedPreferences mPref;

    public ThemeManager() {

    }

    public static ThemeManager get() {
        return new ThemeManager();
    }

    public int getIconSize(Context context) {
        String value = getPrefs(context).getString(context.getString(R.string.pref_icon_size), context.getString(R.string.pref_icon_size_default));
        return Integer.valueOf(value);
    }

    public int getWorkspaceRowCount(Context context) {
        String value = getPrefs(context).getString(context.getString(R.string.pref_row_count), context.getString(R.string.pref_row_count_default));
        return Integer.valueOf(value);
    }

    public int getWorkspaceColCount(Context context) {
        String value = getPrefs(context).getString(context.getString(R.string.pref_col_count), context.getString(R.string.pref_col_count_default));
        return Integer.valueOf(value);
    }

    public int getDockColCount(Context context) {
        String value = getPrefs(context).getString(context.getString(R.string.pref_dock_count), context.getString(R.string.pref_dock_count_default));
        return Integer.valueOf(value);
    }

    public int getDockIconSize(Context context) {
        return getIconSize(context);
    }

    public int getAppsDrawerThemeColor(Context context) {
        boolean theme = getPrefs(context).getBoolean(context.getString(R.string.pref_dark_mode), false);
        boolean themeBlack = getPrefs(context).getBoolean(context.getString(R.string.pref_black_mode), false);
        if(theme) {
            return ContextCompat.getColor(context, R.color.all_apps_container_color_dark);
        } else if(themeBlack) {
            return ContextCompat.getColor(context, R.color.all_apps_container_color_black);
        } else {
            return ContextCompat.getColor(context, R.color.all_apps_container_color);
        }
    }

    private SharedPreferences getPrefs(Context context) {
        if(mPref == null) {
            mPref = context.getSharedPreferences(LauncherFiles.SHARED_PREFERENCES_KEY, Context.MODE_PRIVATE);
        }
        return mPref;
    }

}
