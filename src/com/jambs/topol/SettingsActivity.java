/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jambs.topol;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.ContentObserver;
import android.os.Bundle;
import android.os.Handler;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.SwitchPreference;
import android.provider.Settings;
import android.provider.Settings.System;
import android.support.design.widget.Snackbar;
import android.view.View;

import com.jambs.topol.config.FeatureFlags;

/**
 * Settings activity for Launcher. Currently implements the following setting: Allow rotation
 */
public class SettingsActivity extends Activity implements FragmentInteraction {

    private DevicePolicyManager mDPM;

    private PreferenceSetter mPreferenceFragment;

    private static final int REQUEST_CODE_ENABLE_ADMIN = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_settings);

        // Display the fragment as the main content.
        LauncherSettingsFragment preferenceFragment = LauncherSettingsFragment.newInstance(this);
        this.mPreferenceFragment = preferenceFragment;

        getFragmentManager().beginTransaction()
                .replace(R.id.fl_container, preferenceFragment)
                .commit();

    }

    @Override
    public void showSnackbar(String message) {
        Snackbar snackbar = Snackbar.make(findViewById(R.id.cl_settings), message, Snackbar.LENGTH_INDEFINITE);

        snackbar.setAction(getString(R.string.restart_action), new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                restartApp();
            }
        });

        snackbar.show();
    }

    @Override
    public boolean showAdminPrompt(boolean activate) {
        mDPM = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        ComponentName mDeviceAdminSample = new ComponentName(getApplicationContext(), Administrator.class);

        if (activate != mDPM.isAdminActive(mDeviceAdminSample)) {
            if (activate) {
                // Launch the activity to have the user enable our admin.
                Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
                intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, mDeviceAdminSample);
                intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION,
                        getString(R.string.settings_admin_prompt));
                startActivityForResult(intent, REQUEST_CODE_ENABLE_ADMIN);
                // return false - don't update checkbox until we're really active
                return false;
            } else {
                mDPM.removeActiveAdmin(mDeviceAdminSample);
            }
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE_ENABLE_ADMIN:
                FeatureFlags.TOPOL_DOUBLE_TAP_TO_LOCK = resultCode == RESULT_OK;
                mPreferenceFragment.setPreference(getString(R.string.pref_double_tap_lock), resultCode == RESULT_OK);
                break;
        }
    }

    private void restartApp() {
        android.os.Process.killProcess(android.os.Process.myPid());
    }


    /**
     * This fragment shows the launcher preferences.
     */
    public static class LauncherSettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener, Preference.OnPreferenceChangeListener, PreferenceSetter {

        private SystemDisplayRotationLockObserver mRotationLockObserver;

        private SwitchPreference mDoubleTapPreference;

        private FragmentInteraction mListener;

        public static LauncherSettingsFragment newInstance(FragmentInteraction listener) {
            LauncherSettingsFragment launcherSettingsFragment = new LauncherSettingsFragment();
            launcherSettingsFragment.mListener = listener;
            return launcherSettingsFragment;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            getPreferenceManager().setSharedPreferencesName(LauncherFiles.SHARED_PREFERENCES_KEY);
            addPreferencesFromResource(R.xml.launcher_preferences);

            //Setup custom grid preference
            SwitchPreference customGridPref = (SwitchPreference) findPreference(getString(R.string.pref_custom_grid));
            customGridPref.setOnPreferenceChangeListener(this);
            setGridPreferencesEnabled(customGridPref.isChecked());


            // Setup allow rotation preference
            Preference rotationPref = findPreference(getString(R.string.pref_allow_rotation));
            if (getResources().getBoolean(R.bool.allow_rotation)) {
                // Launcher supports rotation by default. No need to show this setting.
                getPreferenceScreen().removePreference(rotationPref);
            } else {
                ContentResolver resolver = getActivity().getContentResolver();
                mRotationLockObserver = new SystemDisplayRotationLockObserver(rotationPref, resolver);

                // Register a content observer to listen for system setting changes while
                // this UI is active.
                resolver.registerContentObserver(
                        Settings.System.getUriFor(System.ACCELEROMETER_ROTATION),
                        false, mRotationLockObserver);

                // Initialize the UI once
                mRotationLockObserver.onChange(true);
                rotationPref.setDefaultValue(Utilities.getAllowRotationDefaultValue(getActivity()));
            }
            mDoubleTapPreference = (SwitchPreference) findPreference(getString(R.string.pref_double_tap_lock));
            mDoubleTapPreference.setOnPreferenceChangeListener(this);
        }

        @Override
        public boolean onPreferenceChange(Preference preference, Object o) {
            if (preference.getKey().equals(getString(R.string.pref_double_tap_lock))) {
                boolean showAdminPrompt = mListener.showAdminPrompt((Boolean) o);
                FeatureFlags.TOPOL_DOUBLE_TAP_TO_LOCK = showAdminPrompt;
                return showAdminPrompt;
            } else if (preference.getKey().equals(getString(R.string.pref_custom_grid))) {
                setGridPreferencesEnabled((Boolean) o);
                return true;
            }
            return false;
        }

        @Override
        public void onResume() {
            super.onResume();
            getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
            if (s.equals(getString(R.string.pref_double_tap_lock)) || s.equals(getString(R.string.pref_allow_rotation))) {
            } else {
                if (s.equals(getString(R.string.pref_dark_mode))) {
                    mListener.showSnackbar(getString(R.string.restart_required));
                } else {
                    mListener.showSnackbar(getString(R.string.restart_required));
                }
            }
        }

        @Override
        public void onPause() {
            super.onPause();
            getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        }

        @Override
        public void onDestroy() {
            if (mRotationLockObserver != null) {
                getActivity().getContentResolver().unregisterContentObserver(mRotationLockObserver);
                mRotationLockObserver = null;
            }
            super.onDestroy();
        }

        @Override
        public void setPreference(String preferenceKey, Object value) {
            SwitchPreference preference = (SwitchPreference) findPreference(preferenceKey);
            preference.setChecked((boolean) value);
        }

        private void setGridPreferencesEnabled(boolean enabled) {
            FeatureFlags.TOPOL_CUSTOM_GRID = enabled;
            findPreference(getString(R.string.pref_col_count)).setEnabled(enabled);
            findPreference(getString(R.string.pref_row_count)).setEnabled(enabled);
            findPreference(getString(R.string.pref_dock_count)).setEnabled(enabled);
        }
    }

    /**
     * Content observer which listens for system auto-rotate setting changes, and enables/disables
     * the launcher rotation setting accordingly.
     */
    private static class SystemDisplayRotationLockObserver extends ContentObserver {

        private final Preference mRotationPref;
        private final ContentResolver mResolver;

        public SystemDisplayRotationLockObserver(
                Preference rotationPref, ContentResolver resolver) {
            super(new Handler());
            mRotationPref = rotationPref;
            mResolver = resolver;
        }

        @Override
        public void onChange(boolean selfChange) {
            boolean enabled = Settings.System.getInt(mResolver,
                    Settings.System.ACCELEROMETER_ROTATION, 1) == 1;
            mRotationPref.setEnabled(enabled);
            mRotationPref.setSummary(enabled
                    ? R.string.allow_rotation_desc : R.string.allow_rotation_blocked_desc);
        }
    }
}

interface FragmentInteraction {
    void showSnackbar(String message);

    boolean showAdminPrompt(boolean activate);
}

interface PreferenceSetter {
    void setPreference(String preferenceKey, Object value);
}
