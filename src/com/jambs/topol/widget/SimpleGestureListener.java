package com.jambs.topol.widget;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by r.adamek on 5/26/17.
 */
/*
Usage:
  myView.setOnTouchListener(new SimpleGestureListener(this) {
    @Override
    public void onDoubleTap(MotionEvent e) {
      Toast.makeText(MainActivity.this, "Double Tap", Toast.LENGTH_SHORT).show();
    }
  });
*/
public class SimpleGestureListener implements View.OnTouchListener {

    private GestureDetector gestureDetector;

    public SimpleGestureListener(Context c) {
        gestureDetector = new GestureDetector(c, new GestureListener());
    }

    public boolean onTouch(final View view, final MotionEvent motionEvent) {
        return gestureDetector.onTouchEvent(motionEvent);
    }

    private final class GestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onDown(MotionEvent e) {
            return false;
        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            SimpleGestureListener.this.onDoubleTap(e);
            return super.onDoubleTap(e);
        }

        @Override
        public void onLongPress(MotionEvent e) {
            SimpleGestureListener.this.onLongPress(e);
            super.onLongPress(e);
        }
    }

    public void onDoubleTap(MotionEvent e) {
        // To be overridden when implementing listener
    }

    public void onLongPress(MotionEvent e) {

    }
}